package com.collection.pp;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;
import java.util.Vector;

public class CollectionListSimplePractice {

	public void practiceSimpleCollection() {

		// Instantiating the ArrayList

		ArrayList<String> list = new ArrayList<String>();
		list.add("Ravi");
		list.add("Shyam");
		list.add("Hari");
		list.add("Krishna");
		list.add("Pramod");
		list.add("Pramod");

		// traversing list through Iterator

		Iterator itr = list.iterator();

		while (itr.hasNext()) {
			System.out.println(itr.next());
		}

	}

	public void getSimpleLinkedList() {

		LinkedList<String> link = new LinkedList<String>();
		link.add("Ravi");
		link.add("Shyam");
		link.add("Hari");
		link.add("Krishna");
		link.add("Pramod");
		link.add("Pramod");

		Iterator<String> itr = link.iterator();

		while (itr.hasNext()) {
			System.out.println(itr.next());
		}
	}

	public void getSimpleVector() {

		List<String> veclist = new Vector<String>();

		// store the value
		veclist.add("Ayush");
		veclist.add("Amit");
		veclist.add("Surya");
		veclist.add("Garima");

		// fetch out the data

		Iterator<String> itr = veclist.iterator();

		while (itr.hasNext()) {
			System.out.println(itr.next());
		}
	}

	public void simpleStackpractice() {
		List<String> stack = new Stack<String>();
		stack.add("Punya");
		stack.add("Surya");
		stack.add("Binod");
		stack.add("Ramesh");
		stack.add("Narayan");
		stack.add("Karki");

		Iterator<String> itr = stack.iterator();
		while (itr.hasNext()) {
			System.out.println(itr.next());
		}

	}

	public void simpleStackpractice2() {
		Stack<String> stack = new Stack<String>();
		stack.push("Punya");
		stack.push("Surya");
		stack.push("Binod");
		stack.push("Ramesh");
		stack.push("Narayan");
		stack.push("Karki");
		stack.push("Devraj");
		stack.pop();
		

		Iterator<String> itr = stack.iterator();
		while (itr.hasNext()) {
			System.out.println(itr.next());
		}

	}

}
