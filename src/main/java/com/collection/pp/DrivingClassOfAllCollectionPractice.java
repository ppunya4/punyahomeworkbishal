package com.collection.pp;

public class DrivingClassOfAllCollectionPractice {

	public static void main(String[] args) {

		CollectionListSimplePractice arraylist = new CollectionListSimplePractice();

		arraylist.practiceSimpleCollection();

		System.out.println("value for LinkedList");
		arraylist.getSimpleLinkedList();

		System.out.println("Value for Vector");
		arraylist.getSimpleVector();

		System.out.println("value for Stack");
		arraylist.simpleStackpractice();

		System.out.println("value for stack2");
		arraylist.simpleStackpractice2();

		CollectionQueuePractice queue = new CollectionQueuePractice();
		queue.getPriorityQueue();

	}

}
