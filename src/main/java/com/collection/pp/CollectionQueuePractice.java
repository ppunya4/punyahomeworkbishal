package com.collection.pp;

import java.util.Iterator;
import java.util.PriorityQueue;
import java.util.Queue;

public class CollectionQueuePractice {

	/**
	 * instantiation of Queue Interface and the classes \ Queue<String> Q1 = new
	 * PriorityQueue(); Queue<String> Q1 = new ArrayDeQueue();
	 */

	public void getPriorityQueue() {
		Queue<String> priority = new PriorityQueue<String>();

		priority.add("Amit shrma");
		priority.add("Binod Karki");
		priority.add("Krishna Khanal");
		priority.add("Mukti khanal");
		priority.add("Mukesh Dhakal");

		System.out.println("head" + priority.element());
		System.out.println("head" + priority.peek());
		System.out.println("Iterating the queue");
		Iterator itr = priority.iterator();
		while (itr.hasNext()) {
			System.out.println(itr.next());
		}

		priority.remove();
		priority.poll();
		System.out.println("After removing the value");
		Iterator<String> itr2 = priority.iterator();
		while (itr2.hasNext()) {
			System.out.println(itr2.next());
		}
	}

}
