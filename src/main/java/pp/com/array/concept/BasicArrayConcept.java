package pp.com.array.concept;

import java.util.stream.IntStream;

public class BasicArrayConcept {

	int[] arraynumber = new int[] { 20, 30, 50, 4, 71, 99, 100 };

	public int getMaxNumberFromNumber() {

		// assigning the array index to maxnumber
		int maxnumber = arraynumber[0];

		// using the for loop to fetch the array
		for (int i = 0; i < arraynumber.length; i++) {
			// using the condition
			if (arraynumber[i] > maxnumber) {
				maxnumber = arraynumber[i];
			}
		}

		System.out.println("The Given Array Element is:");

		// using for loop to print all given numbers
		for (int i = 0; i < arraynumber.length; i++) {
			System.out.println(arraynumber[i]);
		}

		System.out.println("From The Array Element Largest Number is:" + maxnumber);
		return maxnumber;
	}

	public void getEvenNumbers() {

		System.out.println("Even Numbers:");

		for (int i = 0; i < arraynumber.length; i++) {
			int evenNumber = arraynumber[i];

			if (arraynumber[i] % 2 == 0) {

				System.out.println(evenNumber);
			}

		}

	}

	public void getOddNumber() {
		System.out.println("odd Numbers:");

		int oddNumber;

		for (int i = 0; i < arraynumber.length; i++) {

			oddNumber = arraynumber[i];
			if (arraynumber[i] % 2 == 1) {

				System.out.println(oddNumber);
			}

		}

	}

	public void getSumNumbers() {

		int sum = IntStream.of(arraynumber).sum();
		System.out.println("The sum is " + sum);

	}

	public void getMeanValue() {

		double sum = 0;

		for (int i = 0; i < arraynumber.length; i++) {
			sum += arraynumber[i]; // it is equal to sum = sum+arraynumber

		}
		System.out.println("the mean value is:  " + sum / arraynumber.length);

	}

	public void quitTheProgram() {
		System.exit(0);
	}

}
