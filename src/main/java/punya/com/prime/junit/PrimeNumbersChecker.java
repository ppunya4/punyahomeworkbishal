package punya.com.prime.junit;

public class PrimeNumbersChecker {

	boolean getPrimeNumbers(int numbers) {
		// Corner case
		if (numbers <= 1)
			return false;

		// Check from 2 to n-1
		for (int i = 2; i < numbers; i++)
			if (numbers % i == 0)
				return false;

		return true;
	}

	public boolean verifySubsTring(String first, String second) {
		String str = "";
		String substr = "";

		str = first;
		substr = second;
		if (str.substring(0) == substr) {
			return false;
		}

		return true;

	}

}
