package punya.com.prime.junit;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class PrimeNumbersCheckerTest {
	PrimeNumbersChecker prime;

	@BeforeMethod
	public void setup() {
		prime = new PrimeNumbersChecker();
		System.out.println("setup the environment");
	}

	@Test(priority = 2, groups = "happy")
	public void primeNumberHappyPathTesting() {
		assertTrue(prime.getPrimeNumbers(2));
		assertFalse(prime.getPrimeNumbers(10));
		assertTrue(prime.getPrimeNumbers(13));
		assertTrue(prime.getPrimeNumbers(17));
		assertFalse(prime.getPrimeNumbers(9));
		assertFalse(prime.getPrimeNumbers(27));
	}

	@Test(priority = 1, groups = "negative")
	public void primeNegativeTesting() {
		assertTrue(prime.getPrimeNumbers(2));
		assertTrue(prime.getPrimeNumbers(10));
		assertFalse(prime.getPrimeNumbers(13));
		assertFalse(prime.getPrimeNumbers(17));
		assertTrue(prime.getPrimeNumbers(9));

	}

	@Test
	public void StringSubsTringTest() {
		assertTrue(prime.verifySubsTring("ram", ""));
		assertTrue(prime.verifySubsTring("ram", "ramsharan"));
		assertTrue(prime.verifySubsTring("ram", "sharan")); // Fix for this test case
		assertTrue(prime.verifySubsTring("ramsharan", "sharan"));
		assertTrue(prime.verifySubsTring("ramsharan", "ramsharan")); // Fix this test case
	}

	@AfterMethod
	public void quit() {
	}
}
