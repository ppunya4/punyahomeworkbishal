package punya.com.testng.listener;

import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

public class TestNGListener implements ITestListener {

	@Override
	public void onTestStart(ITestResult result) {
		System.out.println("Store the result set on: " + result.getName());

	}

	@Override
	public void onTestSuccess(ITestResult result) {
		System.out.println("Please store the result set on: " + result.getName());
	}

	@Override
	public void onTestFailure(ITestResult result) {
		System.out.println("Plese store the result set on: " + result.getName());
	}

	@Override
	public void onTestSkipped(ITestResult result) {

	}

	@Override
	public void onTestFailedButWithinSuccessPercentage(ITestResult result) {

	}

	@Override
	public void onStart(ITestContext context) {

	}

	@Override
	public void onFinish(ITestContext context) {

	}
}
