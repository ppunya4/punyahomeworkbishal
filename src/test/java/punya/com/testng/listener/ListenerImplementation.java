package punya.com.testng.listener;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

@Listeners(punya.com.testng.listener.TestNGListener.class)
public class ListenerImplementation {
	@Test
	public void googleTitle() {
		System.setProperty("webdriver.gecko.driver", "geckodriver.exe");
		WebDriver driver = new FirefoxDriver();
		driver.manage().window().maximize();
		driver.get("http://www.google.com");
		System.out.println(driver.getTitle());
		driver.quit();

	}

	@Test
	public void googleTitleMatch() {
		System.out.println("Test - 2 ");
		Assert.assertTrue(false);
	}

}
